import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { PrintPage } from '../pages/print/print';
import { MyApp } from './app.component';

import { DailyreportPage } from '../pages/dailyreport/dailyreport';
import { BettingtabsPage } from '../pages/bettingtabs/bettingtabs';
import { PayoutPage} from '../pages/payout/payout';
import { CalendarPage} from '../pages/calendar/pages';


@NgModule({
  declarations: [
    MyApp,
    PrintPage,
    DailyreportPage,
    BettingtabsPage,
    PayoutPage,
    CalendarPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'})

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PrintPage,
    DailyreportPage,
    BettingtabsPage,
    PayoutPage,
    CalendarPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
