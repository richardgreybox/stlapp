import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController } from 'ionic-angular';

/**
 * Generated class for the BettingparestabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bettingparestab',
  templateUrl: 'bettingparestab.html',
})
export class BettingparestabPage {

  constructor(public toastCtrl: ToastController, private modal: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BettingparestabPage');
  }

  openModal(){
  const myModal = this.modal.create('ModalPage');

  myModal.present();
}


  showToast(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Your bet were successfully added',
      duration: 2000,
      position: position
    });

    toast.present(toast);
  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Your files were successfully saved',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

  showLongToast() {
    let toast = this.toastCtrl.create({
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatibus quibusdam eum nihil optio, ullam accusamus magni, nobis suscipit reprehenderit, sequi quam amet impedit. Accusamus dolorem voluptates laborum dolor obcaecati.',
      duration: 2000,
    });
    toast.present();
  }

}
