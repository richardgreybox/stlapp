import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BettingparestabPage } from './bettingparestab';

@NgModule({
  declarations: [
    BettingparestabPage,
  ],
  imports: [
    IonicPageModule.forChild(BettingparestabPage),
  ],
})
export class BettingparestabPageModule {}
