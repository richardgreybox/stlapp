import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PayoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payout',
  templateUrl: 'payout.html',
})
export class PayoutPage {
	items;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.initializeItems();
  }

  initializeItems() {
    this.items = [
      '2134-4564-4566-4567',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658',
      '3456-9865-8754-7658'

    ];
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad PayoutPage');
  }



}







