import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DailyreportPage} from '../dailyreport/dailyreport';
import { BettingtabsPage} from '../bettingtabs/bettingtabs';
import { PayoutPage} from '../payout/payout';
import { CalendarPage} from '../calendar/pages';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
 
  elevenRoot: any = 'ElevenPage';
  fourRoot: any = 'FourPage';
  nineRoot: any = 'NinePage';



  myIndex: number;
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // Set the active tab based on the passed index from menu.ts
    this.myIndex = navParams.data.tabIndex || 0;
  }

  dailyprintPage()
  {
  this.navCtrl.push(DailyreportPage);
  }
  addbetPage()
  {
  this.navCtrl.push(BettingtabsPage);
  }
  payoutsPage()
  {
  this.navCtrl.push(PayoutPage);
  }

  calendarPage()
  {
  this.navCtrl.push(CalendarPage);
  }
}