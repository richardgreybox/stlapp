import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BettingswertreetabPage } from './bettingswertreetab';

@NgModule({
  declarations: [
    BettingswertreetabPage,
  ],
  imports: [
    IonicPageModule.forChild(BettingswertreetabPage),
  ],
})
export class BettingswertreetabPageModule {}
