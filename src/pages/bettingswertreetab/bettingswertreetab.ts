import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController } from 'ionic-angular';

/**
 * Generated class for the BettingswertreetabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bettingswertreetab',
  templateUrl: 'bettingswertreetab.html',
})
export class BettingswertreetabPage {

  constructor(public toastCtrl: ToastController, private modal: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BettingswertreetabPage');
  }

/**
 * Onchange. Show another content when ramble is selected
 */

	onChange(betoption){
	    if (this.betoption == 'straight') {
	      this.value = 0; 
	    } else if (this.betoption == 'rambled'){
	      this.value = 1; 
	    }
	}

/**
 * Modal Function to view the added bets
 */
  openModal(){
  const myModal = this.modal.create('ModalPage');

  myModal.present();
}

/**
 * Toast that will notify the user when adding bets
 */
  showToast(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Your bet were successfully added',
      duration: 2000,
      position: position
    });

    toast.present(toast);
  }

  showToastWithCloseButton() {
    const toast = this.toastCtrl.create({
      message: 'Your files were successfully saved',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }

  showLongToast() {
    let toast = this.toastCtrl.create({
      message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea voluptatibus quibusdam eum nihil optio, ullam accusamus magni, nobis suscipit reprehenderit, sequi quam amet impedit. Accusamus dolorem voluptates laborum dolor obcaecati.',
      duration: 2000,
    });
    toast.present();
  }

}
