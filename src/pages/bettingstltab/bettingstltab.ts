import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ToastController } from 'ionic-angular';
import { PrintPage } from './../print/print';

/**
 * Generated class for the BettingstltabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bettingstltab',
  templateUrl: 'bettingstltab.html',
})
export class BettingstltabPage {

  constructor(public toastCtrl: ToastController, private modal: ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BettingstltabPage');
  }

  onChange(betoption){
    if (this.betoption == 'straight') {
      this.value = 0; 
    } else if (this.betoption == 'rambled'){
      this.value = 1; 
    }
}

    openModal(){
      const myModal = this.modal.create('ModalPage');
      myModal.present();
    }


  showToast(position: string) {
    let toast = this.toastCtrl.create({
      message: 'Your bet were successfully added',
      duration: 2000,
      position: position
    });

    toast.present(toast);
  }

  load(){
  this.navCtrl.push(PrintPage);
  }
}
