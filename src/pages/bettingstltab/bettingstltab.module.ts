import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BettingstltabPage } from './bettingstltab';

@NgModule({
  declarations: [
    BettingstltabPage,
  ],
  imports: [
    IonicPageModule.forChild(BettingstltabPage),
  ],
})
export class BettingstltabPageModule {}
