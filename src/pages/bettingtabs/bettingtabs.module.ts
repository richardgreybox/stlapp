import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BettingtabsPage } from './bettingtabs';

@NgModule({
  declarations: [
    BettingtabsPage,
  ],
  imports: [
    IonicPageModule.forChild(BettingtabsPage),
  ]
})
export class BettingtabsPageModule {}
