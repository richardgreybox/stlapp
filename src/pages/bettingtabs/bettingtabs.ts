import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-bettingtabs',
  templateUrl: 'bettingtabs.html'
})
export class BettingtabsPage {

  bettingstltabRoot = 'BettingstltabPage'
  bettingswertreetabRoot = 'BettingswertreetabPage'
  bettingparestabRoot = 'BettingparestabPage'


  constructor(public navCtrl: NavController) {}

}


