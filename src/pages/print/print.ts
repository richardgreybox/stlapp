import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BettingtabsPage } from './../bettingtabs/bettingtabs';
/**
 * Generated class for the PrintPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-print',
  templateUrl: 'print.html',
})
export class PrintPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  	console.log(navParams.get('val'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrintPage');
  }

  goPrintBack(){
  this.navCtrl.push(BettingtabsPage);
  }
}
